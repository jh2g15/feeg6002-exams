#include <stdio.h>

double maximum(double *nums);

int main(void)
{
	double numbers[4] = {10.5, 20.2, 19.9, 5.9};
	printf("%.1f\n", maximum(numbers)); 
	return 0;
}

double maximum(double *nums)
{
	double max;
	int i;
		
	max = 0;
	for(i = 0; i < 4; i++)
	{
		if (nums[i] > max)
		{
			max = nums[i];
		}
	}
	
	return max;
}


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM 4

int main (void)
{
	float values[] = {10.7, 13.4, 15.8, 19.2};
	float *valPtr[NUM];
	
	int i;
	srand((unsigned)time(NULL));
	for(i = 0; i < NUM; i++)
	{
		valPtr[i] = &values[(rand() % NUM)];
	}
	
	
	for(i = 0; i < NUM; i++)
	{
		printf("%.1f ", (double) *valPtr[i]);
	}
	
	return 0;
}


#include <stdio.h>
#include <string.h>

struct designData
{
	int numDesigns;
	float data[4];
};

int write_to_file(struct designData desData)
{
	FILE *file1;
	FILE *file2;
	FILE *file3;
	
	file1 = fopen("design1.txt", "w");
	file2 = fopen("design2.txt", "w");
	file3 = fopen("design3.txt", "w");
	
	
	fprintf(file1, "Input script for design %d\n", 1);
	fprintf(file1, "Length of strut %d is %3.1f mm",1, desData.data[0]);
	
	fprintf(file2, "Input script for design %d\n", 1);
	fprintf(file2, "Length of strut %d is %3.1f mm",2, desData.data[1]);
	
	fprintf(file3, "Input script for design %d\n", 1);
	fprintf(file3, "Length of strut %d is %3.1f mm",3, desData.data[2]);
	
	fclose(file1);
	fclose(file2);
	fclose(file3);
	
	return 0;
}

struct designData read_from_file(void)
{
	FILE *file;
	struct designData desData;
	
	file = fopen("designData.txt", "r");
	
	desData.numDesigns = 3;
	
	fscanf(file, "%f %f %f", &desData.data[0], &desData.data[1], &desData.data[2]);
	
	fclose(file);
	
	return desData;
}

int main(void)
{
	struct designData desData;
	desData = read_from_file();
	write_to_file(desData);
	return 0;
}


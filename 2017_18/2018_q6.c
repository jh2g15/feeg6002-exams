#include <stdio.h>

#define NUM_PER_LINE 5
#define N 120
int main(void)
{
	int x = 0;
	
	while(x < N)
	{
		if (x % 5 == 0 && x != 0)
		{
			printf("\n%d", x);
		}
		else
		{
			printf("%d", x);
		}
		x++;
	}
	
	return 0;
}


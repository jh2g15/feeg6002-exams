#include <stdio.h>

char grade(double score)
{
	if(score >= 90.0)
	{
		return 'A';
	}
	else if (score >= 80.0)
	{
		return 'B';
	}
	else if (score >= 70.0)
	{
		return 'C';
	}
	else if (score >= 60.0)
	{
		return 'D';
	}
	else
	{
		return 'F';
	}
}

int main(void)
{
	printf("%c\n",grade(54.));
	printf("%c\n",grade(100.));
	return 0;
}


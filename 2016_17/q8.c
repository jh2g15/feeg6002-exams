#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1000

char *center(char *s, int n)
{
	int i, s_length, left_dash;
	char ret_str[MAX_LEN];
	
	s_length = 0;
	i = 0;
	while(s[i] != '\0')
	{
		i++;
	}
	s_length = i;
	
	left_dash = (n - s_length) / 2;
	
	s_length--;
	
	for(i = 0; i < n; i++)
	{
		if(i < left_dash || i > s_length + left_dash)
		{
			ret_str[i] =  '-';
		}
		else
		{
			ret_str[i]  =s[i - left_dash];
		}
		
	}
	
	
	ret_str[i] = '\0';
	
	return ret_str;
}

int main(void) {
	char s[] = "cat";
	char *ret;
	ret = center(s, 5);
	printf("%s\n", ret);
	free(ret);
	ret = center(s, 6);
	printf("%s\n", ret);
	free(ret);
	ret = center(s, 7);
	printf("%s\n", ret);
	free(ret);
	return 0;
}



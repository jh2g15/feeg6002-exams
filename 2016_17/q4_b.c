#include <stdio.h>

int sum_integers(void);

int main(void)
{
	printf("Sum = %d\n", sum_integers());
	return 0;
}


int sum_integers(void)
{
	int i, answer;
	
	answer = 0;
	
	for(i = 1; i <= 10; i++)
	{
		answer += i;
	}
	
	return answer;
}


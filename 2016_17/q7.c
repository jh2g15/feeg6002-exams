#include <stdio.h>
#include <stdlib.h>

void delete_array(int array[]);
void print(int *a, int n);
int *zero_array(int n);

int main()
{
	int n = 5;
	int *a = zero_array(n);
	print(a, n);
	delete_array(a);
	return 0;
}


void delete_array(int array[])
{
	free(array);
}

int *zero_array(int n)
{
	int i;
	int *ret_pointer;
	ret_pointer = (int *) malloc(sizeof(int) * n);
	for(i = 0; i < n; i++)
	{
		ret_pointer[i] = 0;
	}
	return ret_pointer;
}

void print(int *a, int n) {
	int i = 0;
	for (i=0; i<n; i++) {
		printf("%d ", a[i]);
	}
}


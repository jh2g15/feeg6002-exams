#include <stdio.h>

double compute_mean(double *data, int len);

int main(void)
{
	double data1[6] = {23.2, 31.5, 16.9, 27.5, 25.4, 28.6};
	double data2[6] = {10.0, 20.0};
	printf("The mean is %.1f.\n", compute_mean(data1, 6));
	printf("The mean is %.1f.\n", compute_mean(data2, 2));
	return 0;
}

double compute_mean(double data[], int len)
{
	int i;
	double total_sum, mean;
	
	total_sum = 0;	  
	for(i = 0; i < len; i++)
	{
		total_sum += data[i];
	}
	
	mean = total_sum / len;
	
	return mean;
}


#include <stdio.h>
#include <stdlib.h>

void populate_array(long *array, long num)
{
	int i;
	
	for(i = 0; i < num; i++)
	{
		array[i] = (i * 2) + 2;
	}
}

int main(void) {
	long size = 10;
	long i;
	long *array = malloc(sizeof(long) * size);
	populate_array(array, size);
	for(i=0; i < size; i++) {
		printf("array[%04ld] = %ld\n", i, array[i]);
	}
	free(array);
	return 0;
}


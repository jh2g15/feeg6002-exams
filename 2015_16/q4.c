#include <stdio.h>
#include <math.h>

double standard_deviation(double a, double b, double c, double d)
{
	double mean, std_dev, s1, s2, s3, s4;
	
	mean = (a + b + c + d) / (double) 4;
	
	s1 = (a-mean)*(a-mean);
	s2 = (b-mean)*(b-mean);
	s3 = (c-mean)*(c-mean);
	s4 = (d-mean)*(d-mean);
	
	std_dev = sqrt((1.0/4.0) * (s1 + s2 + s3 + s4));
	
	return std_dev;
}

int main(void) {
	double a, b, c, d, stddev;
	a = 16.3;
	b = 24.2;
	c = 733;
	d = 12.27;
	stddev = standard_deviation(a, b, c, d);
	printf("Standard deviation of %f, %f, %f, %f is %f\n", a, b, c, d, stddev);
	return 0;
}

